#!/usr/bin/fish

if test 1 -gt (count $argv)
    echo "Usage: "(status -f)" DURATION"
    exit 1
end

sleep $argv[1] || exit $status

notify-send -u critical "Times up"\n"Times up"\n"Times up"\n"Elapsed :"$argv[1]

